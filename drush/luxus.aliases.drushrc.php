<?php

$aliases["dev"] = array (
  'root' => '/srv/www/htdocs/luxus2',
  'uri' => 'http://localhost/luxus2',
  'command-specific' => array(
    'core-rsync' => array(
      'delete' => TRUE,
      'omit-dir-times' => TRUE,
    ),
  ),
);

$aliases["live"] = array (
  'root' => '/mnt/data/accounts/g/gembakaizen/data/www/luxus2',
  'uri' => 'http://luxus2.gemba.cz',
  'remote-host' => 'ssh.gembakaizen.savana-hosting.cz',
  'remote-user' => 'user',
  'ssh-options' => '-p 9037',
  'path-aliases' => array(
    '%drush-script' => '/container/home/bin/drush',
  ),
);

