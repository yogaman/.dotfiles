set nocompatible
filetype plugin indent on

" enable per project .vimrc files
set exrc

" map 'leader' an 'localleader' to prepend before commands
let maplocalleader = ","
let mapleader = ",,"

" vim-plug plugin manager section
call plug#begin('~/.vim/plugged')
Plug 'git@github.com:preservim/tagbar.git'
let g:tagbar_position = 'left'
let g:tagbar_autofocus = 1
nmap <F8> :TagbarToggle<CR>

Plug 'https://github.com/scrooloose/nerdtree'
"make NERDTree pop up on the right side
"let g:NERDTreeWinPos = 'right'
"shortcut to toggling nerdtree window
map <F12> :NERDTreeToggle<CR>
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

Plug 'https://github.com/tpope/vim-fugitive'
set statusline=%<%f\ %h%m%r%{FugitiveStatusline()}%=%-14.(%l,%c%V%)\ %P
set laststatus=2
Plug 'git@github.com:tpope/vim-rhubarb.git'
" Plug 'git://github.com/altercation/vim-colors-solarized.git'
Plug 'https://github.com/chrisbra/matchit'
Plug 'git@github.com:tbazant/xml.vim.git', { 'for': 'docbk' }
Plug 'git@github.com:openSUSE/vim-suse-vale-styleguide.git'
let vale_stylecheck_qfwindow = 1
let vale_debug = 1
let vale_log_file = '/tmp/vim-vale.log'
let vale_executable = '/home/tbazant/bin/vale'
Plug 'git@github.com:tbazant/vim-daps.git', { 'for': 'docbk' }
let daps_xml_schema_map = { '.asm.xml': 'assembly', '.xml': 'geekodoc5' }
let daps_dcfile_autostart = 0
let daps_dcfile_glob_pattern = 'DC-'
"let daps_doctype = 'geekodoc5'
"let daps_doctype = 'docbook50'
let daps_xmlformat_script = '/usr/bin/xmlformat'
let daps_xmlformat_conf = '/home/tbazant/.config/daps/docbook-xmlformat.conf'
let daps_auto_import_xmlids = 0
let daps_debug = 1
let daps_log_file = '/tmp/vim_daps.log'
"let daps_executable =  '/home/tbazant/devel/daps/bin/daps'
"let daps_dapsroot = '/home/tbazant/devel/daps'
"let daps_styleroot = '/home/tbazant/devel/suse-xsl/suse2022-ns'
Plug 'git@github.com:tbazant/vim-docbook_templates', { 'for': 'docbk' }
Plug 'Exafunction/codeium.vim', { 'branch': 'main' }
call plug#end()

" general vim customization
syntax on
set ruler
set ignorecase
set smartindent
set wildmenu
set wildmode=list:longest,full
set number
set showmatch
set incsearch
set expandtab
set splitbelow
":vsplit open new file on the right
set splitright
" specify something big to keep the cursor in the vertical middle of the
" screen
set scrolloff=5
"set mouse=a

" general indent settings
set tabstop=2 shiftwidth=2 softtabstop=2 expandtab

" set right margin line
set colorcolumn=80
highlight ColorColumn ctermbg=black guibg=black

" set filetypes for unknows suffixes
au BufRead,BufNewFile *.module set filetype=php
au BufRead,BufNewFile *.adoc set filetype=asciidoc

" fix backspace
set bs=2

" switch the tabs
map <S-Right> :tabnext<CR>
map <S-Left> :tabprevious<CR>

" open quickfix links in new tabs
set switchbuf+=usetab,newtab

" map CTRL+Space to context sensitive autocompletion
imap <C-@> <C-X><C-O>
imap <C-Space> <C-X><C-O>

" indent settings for specific filetypes
autocmd FileType perl set tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType sh set tabstop=4 shiftwidth=4 softtabstop=4
autocmd FileType xml,docbk,asciidoc set tabstop=2 shiftwidth=2 softtabstop=2

" text wrap
autocmd FileType xml,docbk,asciidoc set tw=80 fo+=t fo-=l wrap linebreak nolist

" remove trailing whitespaces
" autocmd Filetype xml,docbk,asciidoc :%s/\s\+$//e

" set spelling options
"autocmd FileType docbk,asciidoc set spell spelllang=en,en-suse-doc
autocmd FileType docbk,asciidoc set spell spelllang=en
autocmd FileType docbk,asciidoc highlight SpellBad cterm=underline ctermfg=red ctermbg=none
autocmd FileType docbk silent mkspell! ~/.vim/spell/suse.utf-8.add
autocmd FileType docbk set spellfile=~/.vim/spell/suse.utf-8.add
autocmd FileType docbk set path=./xml/,./articles/,./tasks/,./concepts/,./references/

" Force to use underline for spell check results
augroup SpellUnderline
  autocmd!
  autocmd ColorScheme *
    \ highlight SpellBad
    \   cterm=reverse
    \   ctermfg=red
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
  autocmd ColorScheme *
    \ highlight SpellCap
    \   cterm=Underline
    \   ctermfg=red
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
  autocmd ColorScheme *
    \ highlight SpellLocal
    \   cterm=Underline
    \   ctermfg=red
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
  autocmd ColorScheme *
    \ highlight SpellRare
    \   cterm=Underline
    \   ctermfg=red
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
augroup END

" set color theme
set background=dark
"colorscheme solarized

" Tidy selected lines (or entire file) with _t:
nnoremap <silent> _t :%!perltidy -i=2 -dcsc -kbl=0 -vt=1 -l=160 -ce -q<CR>
" daps specific commands
nnoremap <silent> _dx :DapsXmlFormat<CR>
nnoremap <silent> _die :DapsImportEntities<CR>
nnoremap <silent> _dix :DapsImportXmlIds<CR>
nnoremap <silent> _dp :DapsPdf<CR>
nnoremap <silent> _dh :DapsHtml<CR>
nnoremap <silent> _dbx :DapsBuildXmlFile<CR>
nnoremap <silent> _dv :DapsValidate<CR>
nnoremap <silent> _dvf :DapsValidateFile<CR>
nnoremap <silent> _dot :DapsOpenTarget<CR>
nnoremap <silent> _dor :DapsOpenReferers<CR>
nnoremap <silent> _vs :ValeStylecheck<CR>

" fugitive shortcuts
nnoremap <silent> _gs :Git<CR>
nnoremap <silent> _gp :Git push<CR>

" replaces <..> with DocBook's <replaceable> and capitalizes
noremap _dbr :s~<\([0-9A-Za-z_\-]\+\)>~<replaceable>\U\1\L</replaceable>~g<CR>
" surrounds visual lines with <listitem/><para/>
noremap _dbli :DocbkSurroundListitems 0 trim<CR>
noremap _dbil :DocbkSurroundListitems il trim<CR>

" set tag completion for specific filetypes
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml,docbk set omnifunc=xmlcomplete#CompleteTags

" toggle the paste mode on <F2>
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" highlight trailing whitespaces (except when typing) and spaces before tabs
highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$\| \+\ze\t/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$\| \+\ze\t/
autocmd BufWinLeave * call clearmatches()

" color scheme for vimdiff
highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red

"highlight for the quickfix window
highlight Search ctermbg=grey

" autocomplete phrases made of words joined with dashes
set iskeyword+=\-

" Remap Enter key in insert mode to confirm autocompletion
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"

" activate matchit macro
runtime macros/matchit.vim

let g:vim_ai_chat = {
\  "options": {
\    "model": "gpt-40",
\    "endpoint_url": "https://api.openai.com/v1/chat/completions",
\    "max_tokens": 1000,
\    "temperature": 1,
\    "request_timeout": 20,
\    "enable_auth": 1,
\    "selection_boundary": "",
\  },
\  "ui": {
\    "code_syntax_enabled": 1,
\    "populate_options": 0,
\    "open_chat_command": "preset_below",
\    "scratch_buffer_keep_open": 0,
\    "paste_mode": 1,
\  },
\}
